#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

#include "rdm6300.h"

const char* ssid     = "smekphone";
const char* password = "svkj9096";

#define RDM6300_RX_PIN 2
#define READ_LED_PIN 13

Rdm6300 rdm6300;

HTTPClient http;

String tagId = "";
int pot = 0;
String payload = "0";

void setup() {

  pinMode(A0, INPUT); 
  
  Serial.begin(115200);
  delay(10);
  Serial.println('\n');

  connectToWifi();

  pinMode(READ_LED_PIN, OUTPUT);
  digitalWrite(READ_LED_PIN, LOW);

  rdm6300.begin(RDM6300_RX_PIN);

  Serial.println("Place RFID tag near the rdm6300...");
}

void loop() {
  /* if non-zero tag_id, update() returns true- a new tag is near! */
  if (rdm6300.update()) {
    pot = analogRead(A0);
    tagId = rdm6300.get_tag_id();
    // Serial.println(rdm6300.get_tag_id(), HEX);
    Serial.println(tagId);
    pot = map(pot, 0, 1024, 1, 50);
    Serial.println(pot);
    payload = "user=" + tagId + "&usage=" + String(pot);
    sendPayload(payload);
  }
  
  digitalWrite(READ_LED_PIN, rdm6300.is_tag_near());

  delay(10);
}

void connectToWifi() {

  WiFi.begin(ssid, password);             // Connect to the network
  Serial.print("Connecting to ");
  Serial.print(ssid); Serial.println(" ...");

  int i = 0;
  while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
    delay(1000);
    Serial.print(++i); Serial.print(' ');
  }

  Serial.println('\n');
  Serial.println("Connection established!");  
  Serial.print("IP address:\t");
  Serial.println(WiFi.localIP());
}

void sendPayload(String payload) {
  HTTPClient http;
  
  http.begin("http://192.168.43.168:8000/water");
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  
  int httpCode = http.POST(payload);   //Send the request
  String res = http.getString();                  //Get the response payload
  
  Serial.println(httpCode);   //Print HTTP return code
  Serial.println(res);    //Print request response payload
  
  http.end();
}

